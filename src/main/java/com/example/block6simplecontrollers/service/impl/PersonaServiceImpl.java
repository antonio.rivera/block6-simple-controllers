package com.example.block6simplecontrollers.service.impl;

import com.example.block6simplecontrollers.model.Persona;
import com.example.block6simplecontrollers.repository.PersonaRepository;
import com.example.block6simplecontrollers.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public Persona findByName(String nombre) {
        return personaRepository.findByNombre(nombre).orElse(null);
    }

    @Override
    public Persona userAdd(Persona persona) {
        /* Busco a la persona con ese nombre y si no existe la creo y si ya existe no la creo
        if (personaRepository.findByName(persona.getNombre()).isEmpty()){
            Persona persona1 = personaRepository.save(persona);
            return persona1;
        }
        return null;*/
        persona.setEdad(persona.getEdad()+1);
        personaRepository.save(persona);
        return persona;
    }
}
