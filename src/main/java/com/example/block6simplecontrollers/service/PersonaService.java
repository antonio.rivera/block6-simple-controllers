package com.example.block6simplecontrollers.service;

import com.example.block6simplecontrollers.model.Persona;

public interface PersonaService {
    Persona findByName(String nombre);
    Persona userAdd(Persona persona);
}
