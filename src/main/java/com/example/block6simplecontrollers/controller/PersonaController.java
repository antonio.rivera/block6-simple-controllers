package com.example.block6simplecontrollers.controller;


import com.example.block6simplecontrollers.model.Persona;
import com.example.block6simplecontrollers.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class PersonaController {
    @Autowired
    private PersonaService personaService;

    @GetMapping("/user/{nombre}")
    public ResponseEntity<String> findByName(@PathVariable String nombre){
        String respuesta="Hola ";
        respuesta+=personaService.findByName(nombre).getNombre();
        return ResponseEntity.ok(respuesta);
    }

    @PostMapping("/addUser")
    public ResponseEntity<Persona> addUser(@RequestBody Persona persona){
        Persona persona1 = personaService.userAdd(persona);
        return ResponseEntity.ok(persona1);
    }
}
