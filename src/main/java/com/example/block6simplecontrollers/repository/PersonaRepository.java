package com.example.block6simplecontrollers.repository;

import com.example.block6simplecontrollers.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonaRepository extends JpaRepository<Persona,Long> {
    Optional<Persona> findByNombre(String nombre);
}
